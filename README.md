# io-oidis-wiki v2019.3.0

> Oidis core documentation

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://gitlab.com/oidis/io-oidis-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## History

### v2019.3.0
Initial release.

## License

This software is owned or controlled by Oidis. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright 2019 [Oidis](https://www.oidis.org/)
