# API

The WUI Framework provides the documentation generated from the source code. The documentation can be generated for each project by 
the `wui docs` cmd command at the project root or by the `docs` script in the **bin\batch** or **bin\bash** sub-folders.

The generated documentation includes all public APIs in a well-structured format.

# Examples

There are no direct examples at this time. The main reason is that the WUI Framework is developed and tested on several applications 
covered by a proprietary licence, so the source code can't be distributed. The best examples of using the provided API are the unit 
tests and RuntimeTests which are part of each library. For more information, see the [project structure](ProjectStructure.md) documentation.

## Licence

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the [LICENSE.txt](https://bitbucket.org/wuiframework/com-wui-framework/raw/133a922b2a079179d4670ede4c654920401ac150/LICENSE.txt) file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2018 [NXP](http://nxp.com/)
