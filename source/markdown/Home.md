![WUI Logo](../../resource/graphics/Logo.png)
> Web-based User Interface Framework for creating cross-platform applications based on web technologies

# About
The development of the WUI Framework started as an internal project at Freescale Semiconductor (currently [NXP Semiconductors](http://www.nxp.com)) 
with a proprietary licence. The development of the in-house software started because a framework which would fully satisfy the needs of 
the internally-developed applications did not exist at that time. It became clear that the framework can be used as a general-purpose 
software for the development of cross-platform applications also outside of Freescale, so the licence changed to open-source BSD-3-Clause 
to use the WUI for free and without limitations (use it as needed, just remember to keep the file headers).   

The main focus of the WUI Framework is:

* to provide the tools for rapid development of cross-platform applications (Windows, Linux, Mac OS X, Android, iOS, Windows Phone, Blackberry, web servers (Apache), plugins for Jetbrains, Visual Studio, Eclipse, and so on)
* usage of web technologies with the offline support, operating system connectivity, and built-in enablement of CORS 
* cross-browser compatibility (IE7+, Edge, Firefox, Chrome, Opera, Safari)
* flexibility and scalability
* fully customizable user controls
* external configuration and easy support of localization
* production-ready solutions with a fully automated release process
* runtime independent development of source code—the developer does not have to care about the target platform

## Runtime environments support status

* Web application
    * All major browsers are supported
    * Able to run on any type of web hosting
* Desktop application
    * Windows is fully supported
    * Linux and Mac OS X are supported by CEF + Node.js — full support in beta phase
* Mobile application
    * Supported by PhoneGap
* IDE plugin
    * JetBrains, Visual Studio, NetBeans, and Eclipse supported in beta phase
* Web server
    * fully supported

# Getting started
The WUI Framework and all projects based on this framework strongly depend on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). 
> The WUI Builder is a cross-platform build tool with a command-line interface, but it is fully tested only in the Windows and Linux environments.

For more information about the installation process, see [WUI Framework Installation guide](InstallationGuide.md).

# Project structure
The WUI Framework is composed from five [bitbucket projects](https://bitbucket.org/wuiframework/profile/projects):  

* [WUI Docs](https://bitbucket.org/account/user/wuiframework/projects/WUIDoc)
* [WUI Frontend](https://bitbucket.org/account/user/wuiframework/projects/WUIFE)
* [WUI Backend](https://bitbucket.org/account/user/wuiframework/projects/WUIBE)
* [WUI Tools](https://bitbucket.org/account/user/wuiframework/projects/WUITOOL)

Each project contains several repositories which behave as libraries and are managed by 
the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). 

# Deep dive
Hungry for more information? See these wiki pages:

* [Version control](VersionControl.md)
* [Installation guide](InstallationGuide.md)
* [IDE configuration](IDEConfiguration.md)
* [Project structure](ProjectStructure.md)
* [Coding standards](CodingStandards.md)
* [WUI Builder details](WUIBuilder.md)
* [API and examples](APIandExamples.md)

# Contribution
Any help is greatly appreciated! 

Contact Jakub Cieslar at [jakub.cieslar@nxp.com](mailto:jakub.cieslar@nxp.com?Subject=WUI%20Framework%20Contribution%20Request) 
for more details.

# Issue tracking
Use the [bitbucket issue tracker](https://bitbucket.org/wuiframework/com-wui-framework/issues) to report bugs or request new features.  

# Licence
This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the [LICENSE.txt](https://bitbucket.org/wuiframework/com-wui-framework/raw/133a922b2a079179d4670ede4c654920401ac150/LICENSE.txt) 
file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2018 [NXP](http://nxp.com/)
