# Installation guide
Installation of WUI Builder on Windows is fully automated. On other systems the WUI Builder requires a toolchain described in detail in the 
[WUI Builder README.md](https://bitbucket.org/wuiframework/com-wui-framework-builder) file. For an easy-to-use initialization 
of the developer environment installation of all prerequisites for current project type (TS, PHP, CPP, etc.) is defined in **selfinstall**
task in WUI Builder. 

The WUI Builder was tested with Windows (7, 8.1, and 10), Linux (Ubuntu 16.04, 17.04, 18.04).
but it should run on any OS.

**Read the content of this installation guide carefully before starting the installation.** 

## Automated installer for Windows
Installation process on Windows is fully automated by [WUI Builder installer](https://cloud.wuiframework.com/index.php/s/MiJw3HWlt276K6Z/download), 
so you just need to download and run installer.

## Manual installation on Linux and Mac OS X
Only a few prerequisites have to be installed manually before running any WUI Builder task:

* [Git](https://git-scm.com/downloads)
* [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder)
* [Node.js](https://nodejs.org/en/download/current)

This guideline focuses on the installation of prerequisites that are missing on the target machine. 

1. Download [Git](https://git-scm.com/downloads) and install them into your system.

    * Linux - **apt-get install git**
    * Mac OS X - **brew install git**
 
2. Clone [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder).


    git clone https://bitbucket.org/wuiframework/com-wui-framework-builder


In workspace directory. Clone will create ./com-wui-framework-builder subdirectory (used below as <wui-builder-path> symbol).
    
3. Download [Node.js](https://nodejs.org/en/download/current) archive (.tar.xz, .tar.gz) for your operating system,
and unpack archive content into:


    <wui-builder-path>/external_modules/nodejs
    
    
4. Run selfinstall task


    <wui-builder-path>/cmd/wui.sh selfinstall

**That is all.**


## Licence

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the [LICENSE.txt](https://bitbucket.org/wuiframework/com-wui-framework/raw/133a922b2a079179d4670ede4c654920401ac150/LICENSE.txt) file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2018 [NXP](http://nxp.com/)
