# Version Control
Each project or library is identified by a unique version. The changeset is tagged by that version at the appropriate commit time. 

The version format is **a.b.c**, where:

    a = year iteration
    b = quartal iteration
    c = release iteration
    
For internal releasing or development purposes **alpha** or **beta** builds could be used, in that cases [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder)
requires special mark in version string (commonly overridden in private.conf.json of demand project) defined as:

    a.b.c-alpha or a.b.c-beta

## Version increase check list
Before each version increase, validate the following:

* Update WUI dependencies before if required
* Update README.md
    * Update the version
    * Update the history
* Update package.conf.json
* Update SW-Content-Register.txt
* Validate all usages as a dependency and fix mainly the SW-Content-Register.txt file

For version update management can be used builder task `wui grunt version-update`. This interactive task will automatically synchronize 
SW-Content-Register.txt and make changes in all required files with expected format based on provided information.

> NOTE: SW-Content-Register (SCR) can contain other WUI dependencies with theirs versions. This is reason why WUI dependencies 
have to be updated first to avoid inconsistency in packages versioning, otherwise project release can contains SCR with older versions.

# Releasing strategy
WUI projects and its repositories are designed with Continues integration and delivery in mind. 
This is reason why you can see at least tree, branches at each repository:

* master 
    - most stable version of the project, with base-lined versions of project dependencies, 
    - on master branch are also created version tags
    - from this branch are released stable Beta, Alpha and GA releases
    - to this branch have write access only project owner or master integrator
* develop 
    - commonly used for integration of new features from feature branches
    - on this branch can be also committed clean ups provided by integrators
    - only from this branch are allowed to create new feature, bugfix or test branches
    - to this branch have write access only project integrators
    - from this branch are release Nightly builds    
* release
    - this branch is used for merging process from develop to master
    - as part of releasing process are created intermediate release-{version} branches used in process of EAP testing
    - from this branch are released EAP builds
    - to this branch are committed hot fixes based on bugs identified in process of UAT testing
* feature, bugfix, test /{JIRA-ID}
    - this branches are used for each new task
    - branch should be prefixed by feature, bugfix or test based on task category
    - branch should be named by JIRA-ID, so it can be easily identified by integrators or each developer
    - this branches are removed from repository immediately after successful merge with develop branch

From each branch can be release several release packages based on project configuration, but each package will have same quality. 
Each package is automatically delivered after successful integration and build. Testing of release packages can be omitted only develop branch. 

## Licence

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the [LICENSE.txt](https://bitbucket.org/wuiframework/com-wui-framework/raw/133a922b2a079179d4670ede4c654920401ac150/LICENSE.txt) file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2018 [NXP](http://nxp.com/)
