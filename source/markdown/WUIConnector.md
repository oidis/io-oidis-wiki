# WUI Connector 

## Linux service

Service is created by systemctl approach.

### Usage
1. Create service


    nano /etc/systemd/system/wui_connector.service

```
[Unit]
 
Description=wui connector
 
 
[Service]
 
ExecStart=/bin/bash /var/wui/com-wui-framework-connector/run.sh
 
User=root
 
 
[Install]
 
WantedBy=multi-user.target
```

2. Create/Edit run.sh


    nano /var/wui/com-wui-framework-connector/run.sh

```
#!/bin/sh
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *
 
echo running wui connector
cd /var/wui/com-wui-framework-connector/
./WuiConnector agent
```

3. Start service


    systemctl start wui_connector.service
    
4. Add service to be started after boot
  
  
      systemctl enable wui_connector.service
      
4. Useful command
  
  
      systemctl status wui_connector.service
      
see status of the service

```
root@imx8qmmek:~# systemctl status wui_connector.service
● wui_connector.service - wui connector
   Loaded: loaded (/etc/systemd/system/wui_connector.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2019-02-13 09:33:02 UTC; 17s ago
 Main PID: 2979 (bash)
    Tasks: 12 (limit: 512)
   CGroup: /system.slice/wui_connector.service
           ├─2979 /bin/bash /var/wui/com-wui-framework-connector/run.sh
           └─2984 ./WuiConnector agent
 
Feb 13 09:33:02 imx8qmmek systemd[1]: Started wui connector.
Feb 13 09:33:02 imx8qmmek bash[2979]: running wui connector
Feb 13 09:33:13 imx8qmmek bash[2979]: INFO: Wed, 13 Feb 2019 09:33:13 GMT at t from t.getInstance
Feb 13 09:33:13 imx8qmmek bash[2979]:     Started sending of monitoring data to: http://ops.wuiframework.com:80
Feb 13 09:33:16 imx8qmmek bash[2979]: INFO: Wed, 13 Feb 2019 09:33:16 GMT at anonymous from c.getCallback
Feb 13 09:33:16 imx8qmmek bash[2979]:     Agent registered.
```

## FFIProxy

### Load

Used for locating and reading library from given path (also resolves relative paths). FFIOptions can be used in more advanced configuration.

```
<FFIOptions>{
    cwd        : "<specify-cwd>",
    env        : {
        path: "<change-any-of-env-variables>"
    },
    libraryPath: "<path-to-library>.dll/.so")
}
```

### Invoke

* Function name is case-sensitive and needs to precisely match function symbol in native library
* Last argument is a variable callabck. In "void foo()" case is $returnValue:any (string) and "void" is detected using 
LiveContentArgumentType.RETURN_VOID

```
($returnValue : number, <args...>) : void => {
    ....
});
```
* Arguments can be passed to function in more than one way
  * a value entered in JS (number, boolean, string) will automatically be translated to an input argument (int/double, bool, char*)
  * FFIArgument object definition, where either the value (that is translated as mentioned above) or both type and value are entered. 
  This is also the only way to use the output arguments of the function (int *, bool *, ...). In the case of a string (char *), 
  a value defining the buffer size (initialized to '0') can be entered into the value
* struct type is currently supported only as an input one
* callback argument can contain any primitive data type bool, int, float, double => JS => boolean, number, string is currently not supported
* structure declaration and function pointer type (callback) have to be available in native code and listed under precise name in 
FFIArgument.value.typeName definition

## Licence

This software is owned or controlled by NXP Semiconductors. 
Use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the [LICENSE.txt](https://bitbucket.org/wuiframework/com-wui-framework/raw/133a922b2a079179d4670ede4c654920401ac150/LICENSE.txt) file for more details.

---

Author Vitezslav Cip, 
Copyright (c) 2019 [NXP](http://nxp.com/)
