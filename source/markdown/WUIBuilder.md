# WUI Builder 

## Main description

The main description of the provided CLI and the list of prerequisites is located in the WUI Builder 
[README.md](https://bitbucket.org/wuiframework/com-wui-framework-builder). 

## Features
> This is a brief description of the functionality provided by the WUI Builder:
* Cross-platform (Windows, Linux, Mac OS X)—based on Node.js and Grunt
* Batch and Bash files interface
* Selfinstall—automated update of the builder itself
* Selfcheck—warning about the selfinstall need
* Autoload of the Grunt plugins and custom scripts
* Support for the global and local instantiations
* Build and test tasks with the profiles' support (dev, prod, and so on)
* Clean build for the production mode—check the GIT status and discard the dependencies modification before the prod build
* Dependencies manager
    * Recursive install/update of the dependencies from the GIT repositories with a support of the desired version specification
    * http, https, and ssh repository URL and local path support
    * Support of the GIT archive exclude by the specification of the GIT attributes
* Compilators       
    * TypeScript to JS with an automatic creation of the reference.d.ts files and an automatic resolving of the files' dependencies
    * SASS to CSS
* Linting for TypeScript, SASS, JS, PHP, C++
* JS and CSS minification with the banner prepend support (import of the licence text)
* JS obfuscation
* File system handling       
    * Cleanup of the build folder and compilation files
    * Resources handling
    * Creation of the ZIP archive from the build target for an easy deployment
    * Automated synchronization with localhost
* `@var` replacement and file `@import` with dependencies support for the .js and .html files
* Testing        
    * Unit testing at mocha framework, phpunit, and gtest
    * TypeScript code coverage at runtime in the browser environment
    * Integral testing by selenium
* Generating of documentation from the source code 
* Watch and hot deploy with the live reload support
* Localhost instantiation
* Support for PHP and C++
* Integration of boost, CMake, and Mingw

and a lot more ...

## OS support status

* Windows
    * Fully supported
* Linux, 
    * Installer script and CLI fully supported
* Mac OS X
    * Installer script and CLI in the beta version
    
## External configuration

### package.conf.json
This file provides a brief project description used by the WUI Builder to deliver the customization of the WUI Builder instance 
and flexibility in the specification of the WUI project needs. The configuration is defined in the JSON format with the API described here:

> WARNING: Do not use this example to create a new config. The JSON file format does not support comments, so this configuration fails at the execution time. This is just a description of all options and it does not have working values.

```
{
    // specification of package name, which should be driven by WUI Framework Coding Standards
    "name": "", // e.g. "com-wui-framework-commons"

    // project current version in string format, which should be tracked by VersionControl diagram and tag in repository
    "version": "", // e.g. "1.0.0"
    
    // brief project description
    // project description will be also used as description for generated executable
    "description": "",

    // licence text, which should be used by banner appenders - mainly CSS and JS minification tools
    "license": "/* Copyright (c) 2014-2015 NXP B.V. This software is owned or controlled by " +
    "NXP Semiconductors. The use of this software is governed by the NXP Licence distributed with this material. See the " +
    "LICENSE file for more details.*/",

    // project homepage
    "homepage": "",

    // project author
    "author": {
        "name": "",
        "email": "@"
    },
    
    // list of contributors
    "contributors": [
        {
            "name": "",
            "email": "@"
        }
    ]
    
    // package accessibility role
    // this configuration is currently not in active use, but it can be useful information in case of that developer is missing read/write
    // permissions to the package repository
    "private": true,

    // specification of repository type and address for the project //
    "repository": {
        "type": "",  // should be git, because other types are not currently supported by WUI Builder
        "url": ""    // url path, which can be used for repository clone
    },

    // PHP specific options //
    "localhost": {
        // url path, where is accessible WUILocalhost
        "hostname": "http://localhost.wuiframework.com",
        // TCP port on which is WUILocalhost listening
        "port": 80,
        // file system path, where should be synchronized generated files
        "base": "{WUIBuilder root}/../WUILocalhost/www"
        // directory
        "directory": "",
        // name of windows apache service
        "winServiceName": "WUI_Framework_Apache2.4"
    },
    
    // class which should be used to load project application
    "loaderClass": "Com.Wui.Framework.Commons.Loader",

    // HTTP resolver class which should be used mainly by PHP projects combined with TypeScript front-end
    "resolverClass": ""
    
    // project target configuration //
    "target": {
        // file name for generated executable
        "name": "{project.name}",
        // company name used in executable metadata
        "companyName": "NXP",
        // copyright used in executable metadata
        "copyright": "Copyright (c) 2014-2016 Freescale Semiconductor, Inc., Copyright (c) 2017 NXP",
        // target description
        "description": ""
        // icon used for executable
        "icon": "resource/graphics/icon.ico",
        // specify wui connector remote port
        "remotePort": -1,
        // window width, applicable only for projects with GUI, ingnored otherwise
        "width": 1036,
        // window height, applicable only for projects with GUI, ignored otherwise
        "height": 780,
        // list of files or file system patterns, which should be copied to directory with generated executable or embedded into
        "resources": [
            {
                // path to resource file, files, or directory (supports globbing)
                "input": "",
                // path to destination relative to executable directory. Use tag "/*" to copy file(s) directly to executable directory with
                // the original file name or leave empty for "input" = "output".
                "output": "",
                // flag to embedd resource to executable ("input" is used as unique embedded resource identifier) [default:false]
                "embed": false,
                // copy flag is not neccessary to be specified, copy of resource will be skipped if "embed" is specified.
                "copy": true,
                // every single text resource file is copied with string-replacing by WUI builder task by default. Use this switch
                // to copy this file in unchanged form.
                "skip-replace": false,
                // embedd resource to windows executable resources
                "resx": false
            }
        ],
        // single or multiple targets could be defined to produce demand build(s)
        "platforms": ["web"],
        // request to elevate WUI connector
        "elevate": false,
        // http hash, which specify application start page e.g. #/publicPage
        "startPage": "",
        // configuration for executable sign, use this for final release.
        "codeSigning": {
            "enabled": false,
            "certPath": "",
            "serverConfig": ""
        },
        // ultimate packer for executable configuration, use this for final release to reduce package size.
        "upx": {
            "enabled": false,
            "options": [
                "--compress-resources=0",
                "--compress-icons=0",
                "--compress-exports=0",
                "--strip-relocs=0",
                "--best"
            ]
        }
        // define configuration for SelfExtractor. Use link to configuration on server or SplashScreen object for offline version.
        // Full configuration is described below
        "splashScreen": ""
    },
    
    // deploy server configuration, used for deploy of release configuration, self-update packages, and other online resources
    "deploy": {
        "server": "https://services.wuiframework.com/xorigin",
        "chunkSize": 1024 * 1024 * 1.5, // 1.5 MB
        "name": ""
    },
    
    // define multiple targets to build at one time. Every single release definition will be merged with project target configuration.
    "releases": {
        "public": {
        }
    },
    
    "docs": {
        "systemName": "",
        "copyright": ""
    },
    
    // list of other WUI Projects or third party projects, which will be used as the project dependencies
    // all dependencies will be stored at dependencies folder and managed by WUI Builder
    // all subdependencies for each specified dependency will be managed automatically
    "dependencies": {
        // DESCRIBED BELOW
    }
}
```

> NOTE: All options are listed in the API description, but most of them are OPTIONAL (basically any option with a predefined value).
The WUI Builder configuration is highly dependent on the current WUI Project structure and dependencies. For more examples, 
see the **project.conf.json** files in the [WUI Frontend repositories](https://bitbucket.org/account/user/wuiframework/projects/WUIFE) or 
[WUI Backend repositories](https://bitbucket.org/account/user/wuiframework/projects/WUIBE).

#### Splash screen configuration
Splash screen configuration is available only for targets with **installer** platform definition on **Windows OS**.
The [WUI Framework SelfExtractor](https://bitbucket.org/wuiframework/com-wui-framework-selfextractor) is more generic replacements for 
common known selfextracting archives i.e. [7zip with SFX](http://www.7-zip.org/).

Main improvement lies in online part of SelfExtractor and also in wide range of scalability.

##### Online SelfExtractor
This SelfExtractor configuration had possibility to download configuration from pre-configured link, process it and download also
all defined internal resource files like background image, progressbar image, language dictionary, application package and other resources.
In the case of application package and other resources an archive unpack could be used if required because SelfExtractor primary works
with archived packages.

```
    "splashScreen": "https://services.wuiframework.com/Configuration/com-freescale-gpu-sdk-installer-services/SelfExtractor"
```

An example of splashScreen configuration is located in [com-wui-framework-services](https://bitbucket.org/wuiframework/com-wui-framework-services/raw/f6c8966e76bbe9774e6e87a06e79439bbab82527/resource/configs/SelfExtractor.jsonp).
This configuration in JSONP format will be automatically merged with default one in [com-wui-framework-selfextractor](https://bitbucket.org/wuiframework/com-wui-framework-selfextractor/raw/777701445ad8f67141e9f7673c281da1d5d6c1d0/resource/data/Com/Wui/Framework/SelfExtractor/Configuration/BaseSelfextractor.jsonp).
Configuration merge is based on additive replacement algorithm. It means that property existing in both configurations will be replaced
by property from child configuration. Same rule is used for JSON objects and its properties. Only for arrays base content will be replaced
anyway if exists in both. Otherwise property from child will be added to base. Configuration merge supports also "extendsConfig" property
which can input other configuration into merge so result will be **child + {extendsConfig} + base**, while extendsConfig is processed
recursively.

```
Com.Wui.Framework.SelfExtractor.DAO.Resources.Data({
        // specify configuration interface, necessary for configuration preparation and deployment by WUI Builder
        $interface: "ISelfExtractorConfiguration",
        // configuration version
        version: "1.0.0",
        // specify link to base configuration
        extendsConfig: "",
        
        // *Add GUI configuration, specification see below
        
        // specify list of resources to by downloaded and prepared on runtime
        resources: [
            {
                // specify url to update server or url and leave projectName, releaseName, and platform empty to use external package
                location: "https://services.wuiframework.com/xorigin",
                // project name of demand package
                projectName: "com-wui-framework-services",
                // release name or empty if package on server has not release name
                releaseName: "public",
                // platform can be win32, win64
                platform: "win32",
                // specify true to download only and copy to install path without unpack of archive
                copyOnly: false
            }
        ]
    });
```

>NOTE: Online configuration is in JSONP format mainly to achieve consistence with other configurations across WUI Framework projects.
From the SelfExtractor side JSONP namespace is not validated and comments are stripped.

##### Offline SelfExtractor
This SelfExtractor configuration had possibility to embed each configurations, resources, and application package inside SelfExtractor
executable. Note that all of them will be located in resources part of Windows executable (RESX). This could lead to huge executable file size.

```
    {
        // *Add GUI configuration, specification see below
        
        // specify list of external resources to by embedded into SelfExractor. Note that application package will be embedded
        // by WUI Builder selfextractor task, so leave resources empty if you do not have any special resource.
        "resources": [
            {
                // specify filesystem path to resource
                "location": "",
                // specify true to download only and copy to install path without unpack of archive
                "copyOnly": false,
                // specify type of resource, SelfExtractor supports RESOURCE or ONLINE type.
                "type": ""
            }
        ]
    }
```

>NOTE: There is no suppression on usage of online links used in configurations bud WUI Framework team strictly recommends to not do that.
Embedded resources in online selfextractor are OK if you choose but online links in offline configuration disturbs intention of offline
SelfExtractor.

##### GUI configuration
Configurations described in paragraphs before are SelfExtractor type (offline/online) dependent. Both of them could be extended with GUI
settings described in that paragraph. 

Configuration can contains also localization dictionary strictly in JSONP format, see [default one](https://bitbucket.org/wuiframework/com-wui-framework-selfextractor/raw/777701445ad8f67141e9f7673c281da1d5d6c1d0/resource/data/Com/Wui/Framework/SelfExtractor/Localization/BaseSelfextractorLocalization.jsonp).
For online configuration use link to file on server and for offline specify local filesystem path.

```
    {
        // location of localization dictionary JSONP file
        "localization": "",
        // splash-screen window width
        "width": 900,
        // splash-screen window height
        "height": 600,
        // specify location of background image
        "background": "",
        // specify where to install application, leave empty to use %TEMP%/<application-package-name> (or $TMP on linux)
        "installPath": "",
        // specify application executable name located in installPath
        "executable": "",
        // splash-screen GUI definition
        "userControls": {
            // dynamic label, supports localization, shows status informations and progress
            "label": {
                // text will be overriden with SelfExtractor
                "text": "",
                // text position from left side
                "x": 0,
                // text position from top side
                "y": 0,
                // specify width (necessary for alignment)
                "width": 200,
                // specify height
                "height": 35,
                // justify text in label to "left", "center", or "right"
                "justify": "left",
                // visibility attribute
                "visible": true,
                // text font
                "font": {
                    // font have to exist on target side, recommends to use universal one
                    "name": "Courier New",
                    // font size in pixels
                    "size": 14,
                    // wants bold font?
                    "bold": false,
                    // specify color in RGB or ARGB format, supports also # or 0x prefixes
                    "color": "#ff0000"
                }
            },
            // static label, same configuration as for "label" except labelStatic.text
            "labelStatic": {
                // type whatever you want to show in splashscreen
                "text": "",
                "x": 0,
                "y": 0,
                "width": 200,
                "height": 35,
                "justify": "left",
                "visible": true,
                "font": {
                    "name": "Courier New",
                    "size": 14,
                    "bold": false,
                    "color": "#0050ff"
                }
            },
            // progress bar
            "progress": {
                // specify background image location or color
                "background": "#ffff00",
                // progress bar location from left side
                "x": 0,
                // progress bar location from top side
                "y": 50,
                // progress bar width, should be same as entered image width
                "width": 400,
                // progress bar height, should be same as entered image width
                "height": 20,
                // marquee style disables progress bar foreground resizing base od current progress
                "marquee": false,
                // visibility of progress bar
                "visible": true,
                // attach progress status with system tray progress, only if marquee if false
                "trayProgress": false,
                // specify foreground image location or color
                "foreground": "#ff00ff"
            }
        }
    }
```

>NOTE: Every referenced image could be GIF with maximum frame rate 50 Hz.

#### Dependency configuration
Project dependency is identified by name equals to subfolder in <project>/dependencies/<dependency-name> and could be 
configured in two ways:

* connection string.
* detailed configuration object.

First one is common for WUI framework related projects and second one is necessary for third-party projects which needs
some special dependency preparation or configuration.

##### 1. Connection string
```
"<dependency-name>": "[connection string]"
    // connection string format is:  [repositoryType]+[location]{#[changeset]}
    //
    // where [repositoryType] is one of:
    //    git, dir
    //
    //    git             - remote repository
    //    dir             - locally cloned repository
    //
    // [location] is one of:
    //    ulr, path
    //
    //    url             - http, https or ssh path to remote origin
    //    path            - file system path to locally cloned repository
    //
    // [changeset] is one of:
    //    tag, branch
    //
    //    this switch is OPTIONAL and default value is 'master'
    //
    //    tag             - required repository changeset specified by tag - mainly equal to project version
    //    branch          - required repository branch   
```

**_Example_**
```
"dependencies": {
    "com-wui-framework-commons": "git+https://bitbucket.org/wuiframework/com-wui-framework-commons.git#develop",
    
    // next one
    
    "com-wui-framework-commons": "git+https://bitbucket.org/wuiframework/com-wui-framework-commons.git#2.0.0",
    
    // next one
    
    "com-wui-framework-commons": "dir+D:/<path to workspace>/com-wui-framework-commons",
}
    
```

##### 2. Detailed configuration
Dependency manager can download demand dependency as archive from general URL, clone from GIT or simply use local directory.
After that step install script can be automatically invoked (if specified in configuration). This install script could be written only
in javascript and will be invoked as WUI builder sub-task of 'dependencies-install' task. The install script can contain any usr defined
logic to prepare demand dependency (i.e. build). 
For example please see [boost_install.js](https://bitbucket.org/wuiframework/com-wui-framework-builder/src/85322cd1553966563c575c4f6b17a71253fc9f83/resource/scripts/boost_install.js?at=master&fileviewer=file-view-default).

Every WUI XCPP project is based on CMAKE build configuration system. So, dependency manager will automatically insert user defined
configuration script to main CMAKE configuration. This configuration script can contain any user defined logic to set compiler and linker
properties for demand dependency. For example please see [boost_configure.cmake](https://bitbucket.org/wuiframework/com-wui-framework-builder/src/85322cd1553966563c575c4f6b17a71253fc9f83/resource/scripts/boost_configure.cmake?at=master&fileviewer=file-view-default).

Use of the same <dependency-name> for different dependency is strictly forbidden. Main reason for that restriction is inheritance of
configuration, it means that in project can be specified dependency **boost** and also dependency **com-wui-framework-xcppcommons** which
has also dependency **boost**, so project configuration has higher priority for configuration merge and attributes for scripts will be 
joined if "ignore-parend-attributes" is not set to True.

```
"<dependency-name>": {
    // version of dependency
    "version": "",
    // location specification
    "location": {
        // could be git, dir, or url by default
        "type": "",
        // url or filesystem path (filesystem path could be used only in private.conf.json. Url supports http, https, and file protocols
        // while file protocol has same limitation as filesystem path. In the case of "type": "git" git:// protocol is also supported.
        "path": "",
        // specify branch or tag for dependency on git repository
        "branch": ""
    },
    // if dependency needs some preparation (i.e. build library(ies)) install-script runs directly after download/checkout 
    "install-script": {
        // install script name with .js extension 
        "name": "",
        // directory path to search for installation script.
        //  Default search order:
        //      1) Specified "path" (can be used only in private.conf.json)
        //      2) <project-path>/resource/scripts/<name>
        //      3) <project-path>/dependencies/<folder-name-at-project-dependencies>/**/<name>
        //      4) <project-path>/bin/resource/scripts/<name>
        //      5) <project-path>/**/<name>
        "path": "",
        // custom attributes for install script
        "attributes": [],
        // attributes are joined with base attributes by default, to suppress base attributes specify true
        "ignore-parent-attributes": false
    },
    "configure-script": {
        // configure script name with .cmake extension
        "name": "",
        // directory path to search for configure script.
        // USES SAME RULES AS FOR INSTALL-SCRIPT PATH
        "path": "",
        // custom attributes for configure script
        "attributes": [],
        // attributes are joined with base attributes by default, to suppress base attributes specify true
        "ignore-parent-attributes": false
    }
}
```

**_Example_**
```
"dependencies": {
    "boost": {
        "version": "1.61.0",
        "location": {
            "type": "file",
            "path": "https://dl.bintray.com/boostorg/release/1.64.0/source/boost_1_64_0.zip"
        },
        "install-script": {
            "name": "boost_install.js",
            "attributes": [
                "-all"
            ]
        },
        "configure-script": {
            "name": "boost_configure.cmake",
            "attributes": [
                "atomic",
                "system",
                "filesystem",
                "program_options",
                "thread",
                "date_time",
                "iostreams",
                "zlib",
                "regex",
                "serialization"
            ]
        }
    },
    
    // next one
    
    "nlohmann": {
        "version": "2.1.1",
        "location": {
            "type": "git",
            "path": "https://github.com/nlohmann/json.git#v2.1.1"
        },
        "configure-script": {
            "name": "nlohmann_json_configure.cmake"
        }
    }
}
```

> NOTE: Recommended naming convention for install script is <dependency-name>_install.js, where <dependency-name> is the same as folder name
        ./dependencies/<dependency-name>. Same rule is valid also for configure scripts.

### private.conf.json
This configuration file is ignored by the GIT at the WUI Project root folder and its main purpose is to override **project.conf.json** to 
meet the requirements of each targeted machine, developer environment, and locally-executed testing.

### project-cleanup.conf.json
This configuration file is ignored by the GIT in the WUI Project root folder and its main purpose is to override the default specification 
set by **{WUIBuilder root}/resource/configs/project-cleanup.conf.json**.

> NOTE: The clean-up script is focused mainly on the removal of unused imports, but it can be also used as a refactoring tool. The clean-up script is automatically executed as a part of the WUI Builder install task. For more details about the execution of the clean-up script see [IDE Configuration](IDEConfiguration.md).

```
{
    // write all logs generated by clean up process into the file
    "cleanuplog": true,
    // write all logs generated by clean up process to console
    "verbose": false,
    // path where should be stored output of clean up script
    "targetFolder": "{WUI Project}", // e.g. "D:/__TEMP__/WUIFramework/new-project-cleanup"
    
    // list of file system pattern, which should not be touched by clean up process
    "ignore": [
        "./project-cleanup.conf.json",
        "./.idea/**/*",
        "./*.iml",
        "./bin",
        "./bin/**/*",
        "./dependencies",
        "./dependencies/**/*",
        "./build",
        "./build/**/*",
        "./build_cache",
        "./build_cache/**/*",
        "./node_definitions",
        "./node_definitions/**/*",
        "./LICENSE.txt",
        "./README.md",
        "./log",
        "./log/**/*",
        "./*.log",
        "./resource/**/Cache/**/*"
    ],
    
    // list of file extensions, which should be touched by clean up process
    "readExtensions": [
        ".js",
        ".json",
        ".jsonp",
        ".ts",
        ".css",
        ".scss",
        ".html",
        ".txt",
        ".php"
    ],
    
    // list of file path modifications
    "filePaths": {
        "[sourcePath]": "[outputPath]"
        // e.g. "Com/Wui/Framework": "Com/Company/Project"
    },
    
    // list of string replacement, which should be applied on each file touched by clean up process
    "contentReplacements": {
        "[sourceString]": "[outputString]"
        // e.g. "Copyright (c) 2014-2015": "Copyright 2016"
    }
}
```

## Licence

This software is owned or controlled by NXP Semiconductors. 
Use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the [LICENSE.txt](https://bitbucket.org/wuiframework/com-wui-framework/raw/133a922b2a079179d4670ede4c654920401ac150/LICENSE.txt) file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2018 [NXP](http://nxp.com/)
