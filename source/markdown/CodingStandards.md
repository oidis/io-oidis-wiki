# Coding standards
The specification of the global coding standards for the WUI Framework is important because the WUI Framework is composed of several 
languages and the readability of the source code must be language-agnostic (in this case). 
The formatting and validation of the coding standards is mostly covered by the built-in linting tools which can be executed by the `wui test lint` command.
The source code formatting is mostly driven by the global set of standards defined for each language with minimal modifications 
based on the WUI Framework requirements.

### Global requirements
* The class is in the CamelCase with the upper leading character.
* The fields and methods are in the CamelCase.
    * The private fields, protected fields, public fields, private methods, and protected methods have a lower leading character.
    * The public methods have an upper leading character.
* The field can be public, but it is strongly recommended to use the Getter Setter methods instead of the public fields.
* The Setter methods are in the CamelCase with a lower leading character and starting with the word "set". The Setter method must be used only for the write-only fields.
* The Getter methods are in the CamelCase with a lower leading character and starting with the word "get". The Getter method must be used only for the read-only fields.
* The Property methods are in the CamelCase with an upper leading character. The Property method name is fully equal to the private field name which can be set or get by this method.
* The Enum fields are UPPER_CASE_WITH_UNDERSCORE and must be in the Enum namespace. The Enum fields must always behave as constants.

> NOTE: The same standards are valid for the static fields and methods with the appropriate modificators.

### Repository name (lowercase-with-dash):
The WUI Package name must be equal to the repository name which is lowercase with the '-' dashes in this format:

    com-wui-framework-[library name]{-[sublibrary name]}

> For example, "com-wui-framework-commons"

### File system path (CamelCase):
The folder namespace is in the CamelCase, separated by the folder separator. This is the source-code namespace format:

	source/[languageType]/Com/Wui/Framework/[LibraryName]{/[SublibraryName]}{/[SubfolderNames]/…}/[FileName.ext]

> For example, "source/typescript/Com/Wui/Framework/Commons/Primitives/String.ts"

The folder namespace is important for the graphics and data resources for the override protection. This is the graphics namespace format:

	resource/[resourceType]/Com/Wui/Framework/[LibraryName]{/[SublibraryName]}/[InterfaceName]{/[SubfolderNames]/…}/[FileName.ext]

> For example, "resource/graphics/Com/Wui/Framework/Commons/WUILogo.png"

### Source code namespace (CamelCase):
The namespace name is in the CamelCase, separated by '.', '::', or without a separator, but it strongly depends on the language schematics. This is the format:

    Com.Wui.Framework.[LibraryName]{.[SublibraryName]}{.[ClassSubnamespace]}.[ClassName]

The folder namespace structure is not important from the source-code prospective, but it is strongly recommended to use the single-class rule with the PSR-4 structure. The namespace is equal to the file system path preceded by the source-code root folder.

Here are some examples of the source-file root folders:

* source/html
* source/typescript
* test/unit/typescript
* test/unit/php
* resource/graphics
* resource/sass

For more details, see [Project Structure documentation](ProjectStructure.md).

### Code structure driven by the modificators
Each implementation must be sorted according to the modificators. The sorting can be validated by the linting tools. This is the required sorting format:

```
static public field
static protected field
static private field

public field
protected field
private field

static public method
static protected method
static private method

constructor

public method	
protected method
private method
```
    
### Web page title (First Letter Uppercase):

    index.html 
        WUI - [Library Name] Library
    Index.ts 
        WUI - [Library Name] Index
    Other
        WUI - Exception
        WUI - Fatal Error
        WUI - Unit Test
        WUI - Developer Corner
        and so on

### JSDoc page title config (First Letter Uppercase):

    WUI - [Library Name]
 
## Code structure
### File header
Each file must start with the header text. The format and content is language-dependent, but each header must contain the following:

* copyright
* licence text in either of these two possible formats:
    * fully-defined licence text 
    * short licence description with a link to the fully-defined licence
	
> NOTE: The NXP copyright must be preserved, but it can be extended by other copyrights. The licence text can be changed to proprietary.

**PHP**
```
/**
* CLASS DESCRIPTION
*
* @copyright Copyright (c) 2014-2016 NXP
* @license SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause licence for this file is located in the LICENSE.txt file included in this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
* @package CLASS NAMESPACE
*/
```
		
**SASS**
```
// * ********************************************************************************************************* *
// *
// * Copyright (c) 2014-2016 NXP
// * 
// * SPDX-License-Identifier: BSD-3-Clause
// * The BSD-3-Clause licence for this file is located in the LICENSE.txt file included in this distribution
// * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
// *
// * ********************************************************************************************************* *
```
		
**HTML, XML**
```
<!--

Copyright (c) 2014-2016 NXP

SPDX-License-Identifier: BSD-3-Clause
The BSD-3-Clause licence for this file is located in the LICENSE.txt file included in this distribution
or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText

-->
```
	
**Other files**
```
/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause licence for this file is located in the LICENSE.txt file included in this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
```

### TypeScript code structure
These reference files must be listed after the file header:
```
///<reference path='sourceFilesMap.d.ts'/>
```
The JSDoc namespace if it is the first class in the namespace:
```
/**
* @namespace Com.Wui.Framework.Gui
*/
```
The specification of the code namespaces:
```
namespaces Com.Wui.Framework.Gui {
```
Each class must be running in the strict mode:
```
"use strict";
```
The aliases for the external classes can be specified:
```
import ParentLoader = Com.Wui.Framework.Commons.Loader;
```
The JSDoc object typedef must be specified:
```
/**
 * @typedef {Object} Loader
 */
```
The JSDoc class description if the constructor was not specified:
```
/**
 * @class Loader
 * @classdesc Loader class handles the web content
 * @memberOf Com.Wui.Framework.Gui
 */
```
Implementation of the TypeScript class:
```
export class Loader extends ParentLoader {

	public static Load() : void {
		
	}
}
```

> NOTE: see the full example of source/typescript/Com/Wui/Framework/Gui/Loader.ts in the 
[repository](https://bitbucket.org/wuiframework/com-wui-framework-gui/src/1ac818c94a5462f17c0db0fc0beed78b29deec46/source/typescript/Com/Wui/Framework/Gui/Loader.ts?at=master&fileviewer=file-view-default).

### SASS code structure			
These reference files must be listed after the file header:
```
@import "../sourceFilesMap";
```
The specification of the code namespaces:
```
.#{$ComWuiFrameworkUserControlsPrimitives} {
```
The implementation of the SASS class:
```
.Panel .Async .Label {
    clear: both;
    left: -10px;
}
```
> NOTE: see the full example of resource/sass/Com/Wui/Framework/UserControls/Primitives/BasePanel.scss in the
[repository](https://bitbucket.org/wuiframework/com-wui-framework-usercontrols/src/8a3c7ba00df6cbd146a418d50b1d50eb7bfbd16c/resource/sass/Com/Wui/Framework/UserControls/Primitives/BasePanel.scss?at=master&fileviewer=file-view-default).

### PHP code structure
The file must start with the code type definition:
```
<?php
```
The specification of the code namespaces:
```
namespace Com\Wui\Framework\Rest\Commons\Primitives {
```
The aliases for the external classes can be specified:
```
use Com\Wui\Framework\Rest\Commons\Interfaces\IBaseObject;
use Com\Wui\Framework\Rest\Commons\Utils\Reflection;
```
Then header file description:
```
/**
* BaseObject class provides ....
*
*/
```
The implementation of the PHP class:
```
class BaseObject implements IBaseObject
{

    public function __construct()
    {

    }
}
```
    
> NOTE: see the full example of source/php/Com/Wui/Framework/Rest/Commons/Primitives/BaseObject.php in the
[repository](https://bitbucket.org/wuiframework/com-wui-framework-rest-commons/src/416da0632a832f71070a50295bcc795deb754296/source/php/Com/Wui/Framework/Rest/Commons/Primitives/BaseObject.php?at=master&fileviewer=file-view-default).
		
### C++ code structure			
#### hpp file
The hpp singleton loader must be defined after the file header:
```
#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_STRING_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_STRING_HPP_
```
The reference files must be listed:
```
#include "../sourceFilesMap.hpp"
```
The aliases for the external classes CAN NOT be specified in class .hpp file. Instead use typedef in a beginning 
of the class declaration:
```
class SomeClass {
    typedef Namespace::OtherClass OtherClass;
 public:
```
The specification of the code namespaces:
```
namespace Com::Wui::Framework::XCppCommons::Primitives {
    class String {
    };
}
```
The implementation of the C++ class header:
```
public:
   /**
    * @return Returns the new line in the selected format.
    */
   static string NewLine();
```
The header file must end with the closure of the hpp singleton loader:  
```
#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_STRING_HPP_
```
    
> NOTE: see the full example of source/cpp/Com/Wui/Framework/XCppCommons/Primitives/String.hpp in the 
[repository](https://bitbucket.org/wuiframework/com-wui-framework-xcppcommons/src/73c7bca5433c3ca3f2ed8375023f82bf8d2dfd78/source/cpp/Com/Wui/Framework/XCppCommons/Primitives/String.hpp?at=master&fileviewer=file-view-default).

#### cpp file
The reference files must be listed after the file header:
```
#include "../sourceFilesMap.hpp"
```
The aliases for the external classes can be specified:
```
using std::string;
```
The implementation of the C++ class:
```
namespace Com::Wui::Framework::XCppCommons::Primitives {
    string String::NewLine(bool $htmlTag /* = true */) {
        return "";
    }
}
```

> NOTE: see the full example of source/cpp/Com/Wui/Framework/XCppCommons/Primitives/String.cpp in the 
[repository](https://bitbucket.org/wuiframework/com-wui-framework-xcppcommons/src/73c7bca5433c3ca3f2ed8375023f82bf8d2dfd78/source/cpp/Com/Wui/Framework/XCppCommons/Primitives/String.cpp?at=master&fileviewer=file-view-default).

## Licence

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the [LICENSE.txt](https://bitbucket.org/wuiframework/com-wui-framework/raw/133a922b2a079179d4670ede4c654920401ac150/LICENSE.txt) file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2018 [NXP](http://nxp.com/)
