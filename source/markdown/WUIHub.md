# WUI Hub 

## Information

Use these links to look up current packages available on HUBs

* [https://hub.wuiframework.com](https://hub.wuiframework.com/#/registry)
* [https://hub.eap.wuiframework.com](https://hub.eap.wuiframework.com/#/registry)
* [https://hub.dev.wuiframework.com](https://hub.dev.wuiframework.com/#/registry)

## Installation

1. Prepare VM on GCP (you can use prepared install [script](https://bitbucket.org/wuiframework/com-wui-framework-docker/raw/21cbaa1ab9dfd2f51709cdf9b3cb36aada7d2c6a/resource/GCP/vm-install.sh))
  a. use Ubuntu image
  b. install docker
  c. install docker-compose
  d. install certbot with appache
  e. initialize firewall
2. Generate certificates for required domain
  a. be sure that VM IP is publicly available and domain is properly registered to this IP
  b. go through certbot interactive console
3. Run docker-compose in source folder containing all files available [here](https://bitbucket.org/wuiframework/com-wui-framework-hub/src/develop/source/docker/Com/Wui/Framework/Hub/Linux/) 
or from cloned repository git clone https://bitbucket.org/wuiframework/com-wui-framework-hub.git. (specify demand --branch=<branch-name> before URL if required)


## Licence

This software is owned or controlled by NXP Semiconductors. 
Use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the [LICENSE.txt](https://bitbucket.org/wuiframework/com-wui-framework/raw/133a922b2a079179d4670ede4c654920401ac150/LICENSE.txt) file for more details.

---

Author Michal Kelnar, 
Copyright (c) 2019 [NXP](http://nxp.com/)
