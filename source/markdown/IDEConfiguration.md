# IDE configuration
The WUI Framework is IDE-agnostic because it only provides the command line interface. Any project can be edited in the text editor 
and built by executing the appropriate scripts. But why not to use a fancy IDE?

[JetBrains family](https://www.jetbrains.com/) (mainly IntelliJ markdown.IDEA and CLion) was chosen as the preferred IDE and the configuration 
files for this IDE are also included in each project.   

## IntelliJ markdown.IDEA configuration
### External tools
Go to **File->Settings… (Ctrl+Alt+S) ->External Tools** and specify the new tools with this configuration:

![ExternalTools](../../resource/graphics/IDEA/01_ExternalTools.png)

Each configuration is using program	`wui.cmd` or `wui.sh` and working directory `$ProjectFileDir$`, see list of program args below.

#### Build group
	
**Install**

	install

**Build - dev, skip tests**

	build dev --skip-test
	
**Rebuild - dev, skip tests**

	rebuild-dev-skip-test

**Build - alpha**

	alpha
		
**Build - beta**

	beta
	
**Build - prod**

	prod

#### Deploy group
	
**Deploy package**

	target-deploy

**Deploy configuration**

	config-deploy --file=$FilePathRelativeToProjectRoot$
	
#### Testing group
	
**Unit test**

	test unit
	
**Single Unit test**

	test unit --file=$FilePathRelativeToSourcepath$
	
**Coverage test**

	run-coverage-test
	
**Single Coverage test**

	run-coverage-test --file=$FilePathRelativeToSourcepath$
		
**Test lint**

	test-lint
	
**Selenium test**

	test selenium
	
**Test all**

	test all

#### Utils group
		
**Docs**

	docs
	
**Clean up**

	project-cleanup
	
**Hotdeploy**

	hotdeploy
	
**Run**

	run

**Update builder**

	update
	
### Plugins
Go to **File->Settings… (Ctrl+Alt+S) ->Plugins->Browse repositories...** to choose and install the required plugin.

![ExternalTools](../../resource/graphics/IDEA/05_Plugins.png)

#### Required
* .ignore
* Apache config (.htaccess) support
* BashSupport
* Batch Scripts Support
* Ini4Idea
* Markdown support
* NodeJS
* PHP

![PHP](../../resource/graphics/IDEA/02_PHP.png)

* Python
* scss-lint

    > NOTE: Search for the configuration file in the WUI Builder installation path.
    
![SCSS](../../resource/graphics/IDEA/03_SCSS.png)

#### Useful
* Identifier Highlighter Reloaded

    > Replace Alt+Up and Alt+Down at Keymap settings

### Keymap
* WUI Build

    > F5
    
* WUI Rebuild

    > Shift + F5
    
![Keymap](../../resource/graphics/IDEA/04_Keymap.png)

### Code style
The WUI Framework requires the validation of the code style (linting). By default, the IntelliJ markdown.IDEA uses a different configuration
for the TypeScript than what is expected by the standard linting tools. To meet the required code style, 
the [typescript configuration file](https://cloud.wuiframework.com/index.php/s/p8GafmNEOvihGnO/download) was exported and also in 
the case of CLion [CPP configuration file](https://cloud.wuiframework.com/index.php/s/55yYgGlhXdHBChA/download).

To import the configuration file, go to 
**File->Settings… (Ctrl+Alt+S) ->Editor->Code Style->TypeScript->Manage...->Import...->IntelliJ markdown.IDEA code style XML**, 
and select the downloaded code style or **..->Code Style->C/C++->..** for CLion IDE. Happy coding :) 

![CodeStyle](../../resource/graphics/IDEA/06_CodeStyle.png)

Starting with CLion 2017.2 [Clang-Tidy](http://clang.llvm.org/extra/clang-tidy/) is embedded to help programmers with various checks
and advices. In the case of WUI Framework we prepared [cpp inspections configuration](https://cloud.wuiframework.com/index.php/s/qZW7NXdH0Q31DUY/download) for that checks.

![Inspections](../../resource/graphics/IDEA/08_Inspections.png)

## Other IDE
You may use some other IDE, for example Visual Studio, Eclipse, or any other. Keep in mind that the WUI Framework is composed of several languages which must be supported by the target IDE. 
The main languages which must be supported by the chosen IDE are:

* TypeScript
* SASS
* HTML
* CSS
* JavaScript
* PHP
* C++
* JAVA

## Licence

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the [LICENSE.txt](https://bitbucket.org/wuiframework/com-wui-framework/raw/133a922b2a079179d4670ede4c654920401ac150/LICENSE.txt) file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2018 [NXP](http://nxp.com/)
